﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccelerometerInput : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float x = 0;
        float z = 0;
        bool hasKeyboard = false;
        if (Input.GetKey(KeyCode.UpArrow))
        {
            hasKeyboard = true;
            z += 0.1f;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            hasKeyboard = true;
            z -= 0.1f;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            hasKeyboard = true;
            x -= 0.1f;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            hasKeyboard = true;
            x += 0.1f;
        }
        if (hasKeyboard)
        {
            transform.Translate(x, 0, z);
        }
        else
        {
            transform.Translate(Input.acceleration.x/10.0f, 0, -Input.acceleration.z/10.0f);
        }
    }
}
